<?php

namespace PavimentaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('PavimentaBundle:Default:index.html.twig');
    }
}
