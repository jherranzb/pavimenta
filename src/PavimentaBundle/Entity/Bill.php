<?php

namespace PavimentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Bill
 *
 * @ORM\Table(name="bill")
 * @ORM\Entity(repositoryClass="PavimentaBundle\Repository\BillRepository")
 */
class Bill
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(
     *     name = "name",
     *     type = "string",
     *     length = 100,
     *     nullable = false
     * )
     */
    protected $name;

    /**
     * @var integer
     * @ORM\Column(
     *     name = "cost",
     *     type = "integer",
     *     nullable = true
     * )
     */
    protected $cost;

    /**
     * @var date
     * @ORM\Column(
     *     name = "date",
     *     type = "date",
     *     length = 200,
     *     nullable = true
     * )
     */
    protected $date;

    /**
     * @var boolean
     * @ORM\Column(
     *     name = "payed",
     *     type = "boolean"
     * )
     */
    protected $payed;

    /**
     * @var string
     * @ORM\Column(
     *     name = "description",
     *     type = "string",
     *     length = 255,
     *     nullable = true
     * )
     */
    protected $description;

    /**
     * One Customer has One Cart.
     * @ORM\OneToOne(targetEntity="Task", mappedBy="bill")
     */
    protected $task;

    /**
     * Many Task have One Project.
     * @ORM\ManyToOne(targetEntity="Builder", inversedBy="bills")
     * @ORM\JoinColumn(name="builder_id", referencedColumnName="id")
     */
    protected $builder;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Bill
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set cost
     *
     * @param integer $cost
     *
     * @return Bill
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return int
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Bill
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set payed
     *
     * @param boolean $payed
     *
     * @return Bill
     */
    public function setPayed($payed)
    {
        $this->payed = $payed;

        return $this;
    }

    /**
     * Get payed
     *
     * @return bool
     */
    public function getPayed()
    {
        return $this->payed;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Bill
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set task
     *
     * @param \PavimentaBundle\Entity\Task $task
     *
     * @return Bill
     */
    public function setTask(\PavimentaBundle\Entity\Task $task = null)
    {
        $this->task = $task;

        return $this;
    }

    /**
     * Get task
     *
     * @return \PavimentaBundle\Entity\Task
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * Set builder
     *
     * @param \PavimentaBundle\Entity\Builder $builder
     *
     * @return Bill
     */
    public function setBuilder(\PavimentaBundle\Entity\Builder $builder = null)
    {
        $this->builder = $builder;

        return $this;
    }

    /**
     * Get builder
     *
     * @return \PavimentaBundle\Entity\Builder
     */
    public function getBuilder()
    {
        return $this->builder;
    }
}
