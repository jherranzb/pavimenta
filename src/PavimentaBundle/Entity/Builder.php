<?php

namespace PavimentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Builder
 *
 * @ORM\Table(name="builder")
 * @ORM\Entity(repositoryClass="PavimentaBundle\Repository\BuilderRepository")
 */
class Builder
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(
     *     name = "name",
     *     type = "string",
     *     length = 100,
     *     nullable = false
     * )
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(
     *     name = "phone",
     *     type = "string",
     *     length = 20,
     *     nullable = true
     * )
     */
    protected $phone;

    /**
     * @var string
     * @ORM\Column(
     *     name = "phone2",
     *     type = "string",
     *     length = 20,
     *     nullable = true
     * )
     */
    protected $phone2;

    /**
     * @var string
     * @ORM\Column(
     *     name = "email",
     *     type = "string",
     *     length = 20,
     *     nullable = true
     * )
     */
    protected $email;

    /**
     * @var string
     * @ORM\Column(
     *     name = "identification",
     *     type = "string",
     *     length = 20,
     *     nullable = true
     * )
     */
    protected $identification;

    /**
     * One Project has Many Tasks.
     * @ORM\OneToMany(targetEntity="Task", mappedBy="builder")
     */
    protected $tasks;

    /**
     * One Project has Many Tasks.
     * @ORM\OneToMany(targetEntity="Bill", mappedBy="builder")
     */
    protected $bills;

    /**
     * Many Users have Many Groups.
     * @ORM\ManyToMany(targetEntity="Work", inversedBy="builders")
     * @ORM\JoinTable(name="builders_work")
     */
    protected $works;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tasks = new \Doctrine\Common\Collections\ArrayCollection();
        $this->bills = new \Doctrine\Common\Collections\ArrayCollection();
        $this->works = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Builder
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Builder
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set phone2
     *
     * @param string $phone2
     *
     * @return Builder
     */
    public function setPhone2($phone2)
    {
        $this->phone2 = $phone2;

        return $this;
    }

    /**
     * Get phone2
     *
     * @return string
     */
    public function getPhone2()
    {
        return $this->phone2;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Builder
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set identification
     *
     * @param string $identification
     *
     * @return Builder
     */
    public function setIdentification($identification)
    {
        $this->identification = $identification;

        return $this;
    }

    /**
     * Get identification
     *
     * @return string
     */
    public function getIdentification()
    {
        return $this->identification;
    }

    /**
     * Add task
     *
     * @param \PavimentaBundle\Entity\Task $task
     *
     * @return Builder
     */
    public function addTask(\PavimentaBundle\Entity\Task $task)
    {
        $this->tasks[] = $task;

        return $this;
    }

    /**
     * Remove task
     *
     * @param \PavimentaBundle\Entity\Task $task
     */
    public function removeTask(\PavimentaBundle\Entity\Task $task)
    {
        $this->tasks->removeElement($task);
    }

    /**
     * Get tasks
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTasks()
    {
        return $this->tasks;
    }

    /**
     * Add bill
     *
     * @param \PavimentaBundle\Entity\Bill $bill
     *
     * @return Builder
     */
    public function addBill(\PavimentaBundle\Entity\Bill $bill)
    {
        $this->bills[] = $bill;

        return $this;
    }

    /**
     * Remove bill
     *
     * @param \PavimentaBundle\Entity\Bill $bill
     */
    public function removeBill(\PavimentaBundle\Entity\Bill $bill)
    {
        $this->bills->removeElement($bill);
    }

    /**
     * Get bills
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBills()
    {
        return $this->bills;
    }

    /**
     * Add work
     *
     * @param \PavimentaBundle\Entity\Work $work
     *
     * @return Builder
     */
    public function addWork(\PavimentaBundle\Entity\Work $work)
    {
        $this->works[] = $work;

        return $this;
    }

    /**
     * Remove work
     *
     * @param \PavimentaBundle\Entity\Work $work
     */
    public function removeWork(\PavimentaBundle\Entity\Work $work)
    {
        $this->works->removeElement($work);
    }

    /**
     * Get works
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWorks()
    {
        return $this->works;
    }
}
