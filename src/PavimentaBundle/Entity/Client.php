<?php

namespace PavimentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cient
 *
 * @ORM\Table(name="client")
 * @ORM\Entity(repositoryClass="PavimentaBundle\Repository\ClientRepository")
 */
class Client
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name="name",
     *     type="string",
     *     length=255)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name="phone",
     *     type="string",
     *     length=255,
     *     nullable = true
     *     )
     */
    protected $phone;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name="phone2",
     *     type="string",
     *     length=255,
     *     nullable = true
     *     )
     */
    protected $phone2;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name="email",
     *     type="string",
     *     length=255,
     *     nullable = true
     *     )
     */
    protected $email;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name="identification",
     *     type="string",
     *     length=255,
     *     nullable = true
     *     )
     */
    protected $identification;

    /**
     * One Cliente has Many Projects.
     * @ORM\OneToMany(
     *     targetEntity="Project",
     *     mappedBy="client")
     */
    protected $projects;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->projects = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Cient
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Cient
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set phone2
     *
     * @param string $phone2
     *
     * @return Cient
     */
    public function setPhone2($phone2)
    {
        $this->phone2 = $phone2;

        return $this;
    }

    /**
     * Get phone2
     *
     * @return string
     */
    public function getPhone2()
    {
        return $this->phone2;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Cient
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set identification
     *
     * @param string $identification
     *
     * @return Cient
     */
    public function setIdentification($identification)
    {
        $this->identification = $identification;

        return $this;
    }

    /**
     * Get identification
     *
     * @return string
     */
    public function getIdentification()
    {
        return $this->identification;
    }

    /**
     * Add project
     *
     * @param \PavimentaBundle\Entity\Project $project
     *
     * @return Client
     */
    public function addProject(\PavimentaBundle\Entity\Project $project)
    {
        $this->projects[] = $project;

        return $this;
    }

    /**
     * Remove project
     *
     * @param \PavimentaBundle\Entity\Project $project
     */
    public function removeProject(\PavimentaBundle\Entity\Project $project)
    {
        $this->projects->removeElement($project);
    }

    /**
     * Get projects
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProjects()
    {
        return $this->projects;
    }
}
