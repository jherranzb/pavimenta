<?php

namespace PavimentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Project
 *
 * @ORM\Table(name="project")
 * @ORM\Entity(repositoryClass="PavimentaBundle\Repository\ProjectRepository")
 */
class Project
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $status;

    /**
     * @var string
     */
    private $latitude;

    /**
     * @var string
     */
    private $longitude;

    /**
     * @var \DateTime
     */
    private $startForecast;

    /**
     * @var \DateTime
     */
    private $endForecast;

    /**
     * @var \DateTime
     */
    private $realStart;

    /**
     * @var \DateTime
     */
    private $realEnd;

    /**
     * @var int
     */
    private $budget;

    /**
     * Many Projects have One Client.
     * @ORM\ManyToOne(targetEntity="Client", inversedBy="projects")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    private $client;

    /**
     * One Project has Many Tasks.
     * @ORM\OneToMany(targetEntity="Task", mappedBy="project")
     */
    protected $tasks;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Project
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return Project
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     *
     * @return Project
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     *
     * @return Project
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set startForecast
     *
     * @param \DateTime $startForecast
     *
     * @return Project
     */
    public function setStartForecast($startForecast)
    {
        $this->startForecast = $startForecast;

        return $this;
    }

    /**
     * Get startForecast
     *
     * @return \DateTime
     */
    public function getStartForecast()
    {
        return $this->startForecast;
    }

    /**
     * Set endForecast
     *
     * @param \DateTime $endForecast
     *
     * @return Project
     */
    public function setEndForecast($endForecast)
    {
        $this->endForecast = $endForecast;

        return $this;
    }

    /**
     * Get endForecast
     *
     * @return \DateTime
     */
    public function getEndForecast()
    {
        return $this->endForecast;
    }

    /**
     * Set realStart
     *
     * @param \DateTime $realStart
     *
     * @return Project
     */
    public function setRealStart($realStart)
    {
        $this->realStart = $realStart;

        return $this;
    }

    /**
     * Get realStart
     *
     * @return \DateTime
     */
    public function getRealStart()
    {
        return $this->realStart;
    }

    /**
     * Set realEnd
     *
     * @param \DateTime $realEnd
     *
     * @return Project
     */
    public function setRealEnd($realEnd)
    {
        $this->realEnd = $realEnd;

        return $this;
    }

    /**
     * Get realEnd
     *
     * @return \DateTime
     */
    public function getRealEnd()
    {
        return $this->realEnd;
    }

    /**
     * Set budget
     *
     * @param integer $budget
     *
     * @return Project
     */
    public function setBudget($budget)
    {
        $this->budget = $budget;

        return $this;
    }

    /**
     * Get budget
     *
     * @return int
     */
    public function getBudget()
    {
        return $this->budget;
    }
}
