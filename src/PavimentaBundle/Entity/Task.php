<?php

namespace PavimentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Task
 *
 * @ORM\Table(name="task")
 * @ORM\Entity(repositoryClass="PavimentaBundle\Repository\TaskRepository")
 */
class Task
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(
     *     name = "name",
     *     type = "string",
     *     length = 100,
     *     nullable = true
     * )
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(
     *     name = "description",
     *     type = "string",
     *     length = 255,
     *     nullable = true
     * )
     */
    protected $description;

    /**
     * @var date
     * @ORM\Column(
     *     name = "startForecast",
     *     type = "date",
     *     length = 200,
     *     nullable = false
     * )
     */
    protected $startForecast;

    /**
     * @var date
     * @ORM\Column(
     *     name = "endForecast",
     *     type = "date",
     *     length = 200,
     *     nullable = false
     * )
     */
    protected $endForecast;

    /**
     * @var date
     * @ORM\Column(
     *     name = "realStart",
     *     type = "date",
     *     length = 200,
     *     nullable = false
     * )
     */
    protected $realStart;

    /**
     * @var date
     * @ORM\Column(
     *     name = "realEnd",
     *     type = "date",
     *     length = 200,
     *     nullable = false
     * )
     */
    protected $realEnd;

    /**
     * @var integer
     * @ORM\Column(
     *     name = "budget",
     *     type = "integer"
     * )
     */
    protected $budget;

    /**
     * Many Task have One Project.
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="tasks")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     */
    protected $project;

    /**
     * Many Task have One Builder.
     * @ORM\ManyToOne(targetEntity="Builder", inversedBy="tasks")
     * @ORM\JoinColumn(name="builder_id", referencedColumnName="id")
     */
    protected $builder;

    /**
     * One Cart has One Customer.
     * @ORM\OneToOne(targetEntity="Bill", inversedBy="task")
     * @ORM\JoinColumn(name="task_id", referencedColumnName="id")
     */
    protected $bill;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Task
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Task
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set startForecast
     *
     * @param \DateTime $startForecast
     *
     * @return Task
     */
    public function setStartForecast($startForecast)
    {
        $this->startForecast = $startForecast;

        return $this;
    }

    /**
     * Get startForecast
     *
     * @return \DateTime
     */
    public function getStartForecast()
    {
        return $this->startForecast;
    }

    /**
     * Set endForecast
     *
     * @param \DateTime $endForecast
     *
     * @return Task
     */
    public function setEndForecast($endForecast)
    {
        $this->endForecast = $endForecast;

        return $this;
    }

    /**
     * Get endForecast
     *
     * @return \DateTime
     */
    public function getEndForecast()
    {
        return $this->endForecast;
    }

    /**
     * Set realStart
     *
     * @param \DateTime $realStart
     *
     * @return Task
     */
    public function setRealStart($realStart)
    {
        $this->realStart = $realStart;

        return $this;
    }

    /**
     * Get realStart
     *
     * @return \DateTime
     */
    public function getRealStart()
    {
        return $this->realStart;
    }

    /**
     * Set realEnd
     *
     * @param \DateTime $realEnd
     *
     * @return Task
     */
    public function setRealEnd($realEnd)
    {
        $this->realEnd = $realEnd;

        return $this;
    }

    /**
     * Get realEnd
     *
     * @return \DateTime
     */
    public function getRealEnd()
    {
        return $this->realEnd;
    }

    /**
     * Set budget
     *
     * @param integer $budget
     *
     * @return Task
     */
    public function setBudget($budget)
    {
        $this->budget = $budget;

        return $this;
    }

    /**
     * Get budget
     *
     * @return int
     */
    public function getBudget()
    {
        return $this->budget;
    }

    /**
     * Set project
     *
     * @param \PavimentaBundle\Entity\Project $project
     *
     * @return Task
     */
    public function setProject(\PavimentaBundle\Entity\Project $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \PavimentaBundle\Entity\Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set builder
     *
     * @param \PavimentaBundle\Entity\Builder $builder
     *
     * @return Task
     */
    public function setBuilder(\PavimentaBundle\Entity\Builder $builder = null)
    {
        $this->builder = $builder;

        return $this;
    }

    /**
     * Get builder
     *
     * @return \PavimentaBundle\Entity\Builder
     */
    public function getBuilder()
    {
        return $this->builder;
    }

    /**
     * Set bill
     *
     * @param \PavimentaBundle\Entity\Bill $bill
     *
     * @return Task
     */
    public function setBill(\PavimentaBundle\Entity\Bill $bill = null)
    {
        $this->bill = $bill;

        return $this;
    }

    /**
     * Get bill
     *
     * @return \PavimentaBundle\Entity\Bill
     */
    public function getBill()
    {
        return $this->bill;
    }
}
