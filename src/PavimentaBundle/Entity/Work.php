<?php

namespace PavimentaBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Work
 *
 * @ORM\Table(name="work")
 * @ORM\Entity(repositoryClass="PavimentaBundle\Repository\WorkRepository")
 */
class Work
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name="type",
     *     type="string",
     *     length=255,
     *     nullable = true
     *     )
     */
    protected $type;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name="description",
     *     type="string",
     *     length=255,
     *     nullable = true
     *     )
     */
    protected $description;

    /**
     * Many Groups have Many Users.
     * @ORM\ManyToMany(targetEntity="Builder", mappedBy="works")
     */
    protected $builders;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->builders = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Work
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Work
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add builder
     *
     * @param \PavimentaBundle\Entity\Builder $builder
     *
     * @return Work
     */
    public function addBuilder(\PavimentaBundle\Entity\Builder $builder)
    {
        $this->builders[] = $builder;

        return $this;
    }

    /**
     * Remove builder
     *
     * @param \PavimentaBundle\Entity\Builder $builder
     */
    public function removeBuilder(\PavimentaBundle\Entity\Builder $builder)
    {
        $this->builders->removeElement($builder);
    }

    /**
     * Get builders
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBuilders()
    {
        return $this->builders;
    }
}
